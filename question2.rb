#Firstly, it will check if root is exist, if not it will set the value as a root
#otherwise, it will perform an insert method in Node class which is
#if current node exist and the new node value is less than current node value, 
#it will continue down the branch on the left until the end is reached. 
#There is where the node is setup
#same goes to set the right node

class Tree
        def initialize
            @root = nil
        end


        def insert(value)
            if @root.nil?
                @root = Node.new(value)
            else
                @root.insert(value)
            end
        end

        def self.from_array(arr)
            tree = Tree.new
            arr.map{|x| tree.insert(x)}
            puts tree.inspect
        end

            class Node
                attr_reader :value, :right, :left

                def initialize(value)
                    @value = value
                    @left = left
                    @right = right
                end
                
                def insert(new_value)
                    if new_value < @value
                        @left.nil? ? @left = Node.new(new_value) : @left.insert(new_value)
                        
                    elsif new_value > @value
                        @right.nil? ? @right = Node.new(new_value) : @right.insert(new_value)
                    
                    end
                end
            end

       

end

puts Tree.from_array([78,56,97,21,67,62,81,115])
# #=====RESULT=====
# #<Tree:0x00007f899889a0e8 
#     @root=#<Tree::Node:0x00007f899889a098 @value=78, 
#             @left=#<Tree::Node:0x00007f899889a070 @value=56, 
#                 @left=#<Tree::Node:0x00007f899889a020 @value=21, 
    #                     @left=nil, 
    #                     @right=nil>, 
#                 @right=#<Tree::Node:0x00007f8998899ff8 @value=67, 
    #                     @left=<Tree::Node:0x00007f8998899fd0 @value=62, 
#                             @left=nil, 
#                             @right=nil>, 
    #                     @right=nil>>, 
#             @right=
#                 #<Tree::Node:0x00007f899889a048 @value=97, 
#                     @left=#<Tree::Node:0x00007f8998899fa8 @value=81, 
#                         @left=nil, 
#                         @right=nil>, 
#                     @right=#<Tree::Node:0x00007f8998899f80 @value=115, 
#                         @left=nil, 
#                         @right=nil>>>>
